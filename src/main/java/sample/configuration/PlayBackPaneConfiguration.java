package sample.configuration;

import javafx.event.Event;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.FlowPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sample.AudioPlayer;
import sample.utilities.CustomEventTypes;

import static sample.utilities.Utilities.createButton;

@Configuration
public class PlayBackPaneConfiguration {

    @Autowired
    AudioPlayer audioPlayer;

    private String iconsFolderPath = "icons/";

    @Bean
    public Button playButton(@Qualifier("currentMusicLabel") Label currentMusicLabel) {
        String playButtonImagePath = iconsFolderPath + "play.png";
        Button playButton = createButton(playButtonImagePath);
        Event playButtonPressed = new Event(CustomEventTypes.SHOW_PAUSE_BUTTON);
        playButton.setOnMouseClicked(event -> {
            audioPlayer.getSelectedSong().ifPresent(selectedSong -> audioPlayer.playMusic());
            playButton.fireEvent(playButtonPressed);
            currentMusicLabel.setText(audioPlayer.getPlaylist().getSelectionModel().getSelectedItem().toString());
        });
        return playButton;
    }

    @Bean
    public Button pauseButton() {
        String pauseButtonImagePath = iconsFolderPath + "pause.png";
        Button pauseButton = createButton(pauseButtonImagePath);
        Event playButtonPressed = new Event(CustomEventTypes.SHOW_PLAY_BUTTON);
        pauseButton.setOnMouseClicked(event1 -> {
            audioPlayer.setMusicCurrentTime();
            audioPlayer.getMediaPlayer().pause();
            pauseButton.fireEvent(playButtonPressed);
        });
        return pauseButton;
    }

    @Bean
    public Button stopButton() {
        String stopButtonImagePath = iconsFolderPath + "stop.png";
        Button stopButton = createButton(stopButtonImagePath);
        Event stopButtonPressed = new Event(CustomEventTypes.SHOW_PLAY_BUTTON);
        stopButton.setOnMouseClicked(event -> {
            audioPlayer.getMediaPlayer().stop();
            audioPlayer.stopCurrentTime();
            stopButton.fireEvent(stopButtonPressed);
        });
        return stopButton;
    }

    @Bean
    public Button nextButton(@Qualifier("genreComboBox") ComboBox genreComboBox,
                             @Qualifier("currentMusicLabel") Label currentMusicLabel) {
        String nextButtonImagePath = iconsFolderPath + "next.png";
        Button nextButton = createButton(nextButtonImagePath);
        Event nextButtonPressed = new Event(CustomEventTypes.SHOW_PAUSE_BUTTON);
        nextButton.setOnMouseClicked(event -> {
            audioPlayer.getPlaylist().getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            if (audioPlayer.getPlaylist().getSelectionModel().getSelectedIndex() <
                    audioPlayer.getPlaylist().getItems().size() - 1) {
                audioPlayer.getMediaPlayer().stop();
                audioPlayer.getPlaylist().getSelectionModel()
                        .select((audioPlayer.getPlaylist().getSelectionModel().getSelectedIndex() + 1));
                audioPlayer.playMusic();
            }
            currentMusicLabel.setText(audioPlayer.getPlaylist().getSelectionModel().getSelectedItem().toString());
            nextButton.fireEvent(nextButtonPressed);
            genreComboBox.getSelectionModel().select(audioPlayer.getPathForSelectedSong());
        });
        return nextButton;
    }

    @Bean
    public Button previousButton(@Qualifier("genreComboBox") ComboBox genreComboBox,
                                 @Qualifier("currentMusicLabel") Label currentMusicLabel,
                                 @Value("${musicDataPath}")String musicDataPath) {
        String previousButtonImagePath = iconsFolderPath + "previous.png";
        Button previousButton = createButton(previousButtonImagePath);
        Event previousButtonPressed = new Event(CustomEventTypes.SHOW_PAUSE_BUTTON);
        previousButton.setOnMouseClicked(event -> {
            audioPlayer.getPlaylist().getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            if (audioPlayer.getPlaylist().getSelectionModel().getSelectedIndex() > 0) {
                audioPlayer.getMediaPlayer().stop();
                audioPlayer.getPlaylist().getSelectionModel()
                        .select((audioPlayer.getPlaylist().getSelectionModel().getSelectedIndex() - 1));
                audioPlayer.playMusic();
            }
            currentMusicLabel.setText(audioPlayer.getPlaylist().getSelectionModel().getSelectedItem().toString());
            previousButton.fireEvent(previousButtonPressed);
            genreComboBox.getSelectionModel().select(audioPlayer.getPathForSelectedSong());
        });
        return previousButton;
    }

    @Bean
    public Button replayButton() {
        String replayButtonImagePath = iconsFolderPath + "replay.png";
        Button replayButton = createButton(replayButtonImagePath);
        Event replayButtonPressed = new Event(CustomEventTypes.SHOW_PAUSE_BUTTON);
        replayButton.setOnMouseClicked(event -> {
            audioPlayer.getMediaPlayer().stop();
            audioPlayer.stopCurrentTime();
            audioPlayer.playMusic();
            replayButton.fireEvent(replayButtonPressed);
        });
        return replayButton;
    }

    @Bean
    public FlowPane playBackPane(@Qualifier("previousButton") Button previousButton,
                                 @Qualifier("replayButton") Button replayButton,
                                 @Qualifier("playButton") Button playButton,
                                 @Qualifier("stopButton") Button stopButton,
                                 @Qualifier("nextButton") Button nextButton,
                                 @Qualifier("pauseButton") Button pauseButton) {
        FlowPane playBackPane = new FlowPane(Orientation.HORIZONTAL, 10, 10);
        playBackPane.setColumnHalignment(HPos.CENTER);
        playBackPane.setAlignment(Pos.CENTER);
        playBackPane.getChildren().addAll(previousButton, replayButton, playButton, stopButton, nextButton);
        playBackPane.setPrefSize(250, 25);
        playBackPane.addEventHandler(CustomEventTypes.SHOW_PAUSE_BUTTON, event -> playBackPane.getChildren().set(2, pauseButton));
        playBackPane.addEventHandler(CustomEventTypes.SHOW_PLAY_BUTTON, event -> playBackPane.getChildren().set(2, playButton));
        return playBackPane;
    }

}
