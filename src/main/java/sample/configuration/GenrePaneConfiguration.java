package sample.configuration;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.FlowPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import sample.AudioPlayer;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import static sample.utilities.Utilities.*;

@Configuration
@PropertySource("classpath:application.properties")
public class GenrePaneConfiguration {

    @Autowired
    AudioPlayer audioPlayer;

    @Bean
    public ComboBox genreComboBox() {
        ObservableList<String> genreList = FXCollections.observableArrayList();
        genreList.setAll("Afro", "Blues", "Electronic", "Flamenco", "Folk", "Hip Hop", "Jazz", "Latin",
                "Pop", "R&B and soul", "Rock");
        ComboBox genreComboBox = new ComboBox();
        genreComboBox.setItems(genreList);
        return genreComboBox;
    }

    @Bean
    public Button selectButton(@Qualifier("genreComboBox") ComboBox<String> genreComboBox) {
        Button selectGenreButton = new Button("Select");
        selectGenreButton.setOnMouseClicked(event -> {
            if (genreComboBox.getSelectionModel().getSelectedItem() != null &&
                    audioPlayer.isItemSelected()) {

                audioPlayer.getSelectedSong().ifPresent(selectedSong -> {
                    audioPlayer.createMusicFileTxt(
                            selectedSong,
                            genreComboBox.getSelectionModel().getSelectedItem(),
                            audioPlayer.getFileMap().get(audioPlayer.getPlaylist().getSelectionModel().getSelectedItem())
                    );
                });

                genreComboBox.getSelectionModel().clearSelection();
            }
        });
        return selectGenreButton;
    }

    @Bean
    public FlowPane genrePane(@Qualifier("genreComboBox") ComboBox<String> genreComboBox,
                              @Qualifier("selectButton") Button selectButton) {
        FlowPane genrePane = new FlowPane(Orientation.HORIZONTAL, 10, 10);
        genrePane.setAlignment(Pos.CENTER);
        genrePane.setColumnHalignment(HPos.CENTER);
        genrePane.setMaxWidth(250);
        genrePane.getChildren().addAll(genreComboBox, selectButton);
        return genrePane;
    }


}
