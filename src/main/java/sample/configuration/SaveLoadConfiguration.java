package sample.configuration;

import javafx.event.Event;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sample.AudioPlayer;
import sample.utilities.CustomEventTypes;
import sample.utilities.Utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.stream.Stream;

import static sample.utilities.Utilities.createPlaylistFile;
import static sample.utilities.Utilities.readFromPlaylistFile;
import static sample.utilities.Utilities.showAlert;

@Configuration
public class SaveLoadConfiguration {

    @Bean
    public ComboBox<String> playlistComboBox(@Value("playlistsFilePath") String playlistsFilePath) {
        ComboBox<String> playlistBox = new ComboBox<>();
        if (new File(playlistsFilePath).exists()) {
            File file = new File(playlistsFilePath);
            try {
                readFromPlaylistFile(file, playlistBox);
            } catch (FileNotFoundException e) {
                Utilities.showAlert("File not found");
            }
        }
        return playlistBox;
    }

    @Bean
    public Button ConfirmPlaylistLoad(AudioPlayer audioPlayer, @Qualifier("playlistComboBox") ComboBox playlistComboBox) {
        Button okButton = new Button("Ok");
        Event closeStage = new Event(CustomEventTypes.CLOSE_STAGE);
        okButton.setOnMouseClicked(event -> {
            if (playlistComboBox.getSelectionModel().getSelectedItem() != null) {
                audioPlayer.getPlaylist().getItems().clear();
                File file = new File("D:\\" + playlistComboBox.getSelectionModel().getSelectedItem());
                Stream<File> files;
                files = Stream.of(file.listFiles());
                files.forEach(file1 -> {
                    audioPlayer.filePutFromPlaylist(file1);
                });
            }
            okButton.fireEvent(closeStage);
        });
        return okButton;
    }

    @Bean
    public Stage createLoadPlaylistStage(@Qualifier("playlistComboBox") ComboBox playlistComboBox,
                                         @Qualifier("ConfirmPlaylistLoad") Button okButton) {
        FlowPane flowPane = new FlowPane(Orientation.VERTICAL, 10, 10);
        flowPane.getChildren().addAll(playlistComboBox, okButton);
        flowPane.setColumnHalignment(HPos.CENTER);
        flowPane.setAlignment(Pos.CENTER);
        Scene scene = new Scene(flowPane, 200, 200);
        Stage stage = new Stage();
        stage.setTitle("Select playlist name");
        stage.setScene(scene);
        stage.addEventHandler(CustomEventTypes.CLOSE_STAGE, event -> stage.close());
        return stage;
    }

    @Bean
    public TextField enterPlaylistName() {
        TextField textField = new TextField();
        return textField;
    }

    @Bean
    public Button confirmPlaylistSave(@Qualifier("enterPlaylistName") TextField textField, AudioPlayer audioPlayer,
                                      @Qualifier("playlistComboBox") ComboBox playlistComboBox) {
        Button okButton = new Button("Ok");
        Event closeStage = new Event(CustomEventTypes.CLOSE_STAGE);
        okButton.setOnMouseClicked(event -> {
            if (textField.textProperty().getValue() != null) {
                String playlistName = textField.textProperty().getValue();
                String folderPath = "D:\\" + textField.textProperty().getValue();
                File file = new File(folderPath);
                file.mkdir();
                NumberFormat formater = new DecimalFormat(("#0000000"));
                for (int i = 0; i < audioPlayer.getPlaylist().getItems().size(); i++) {
                    String item = audioPlayer.getPlaylist().getItems().get(i).toString();
                    Path sourceDirectory = Paths.get(audioPlayer.getFileMap().get(item));
                    Path targetDirectory = Paths.get(folderPath + "\\" + formater.format(i) + "-" + item);
                    try {
                        Files.copy(sourceDirectory, targetDirectory);
                    } catch (IOException e) {
                        Utilities.showAlert("IOException");
                    }
                }
                playlistComboBox.getItems().add(playlistName);
                try {
                    createPlaylistFile(playlistName);
                } catch (IOException e) {
                    showAlert("IO exception");
                }
            }
            okButton.fireEvent(closeStage);
        });
        return okButton;
    }

    @Bean
    public Stage createSavePlaylistStage(@Qualifier("enterPlaylistName") TextField textField,
                                         @Qualifier("confirmPlaylistSave") Button okButton) {
        FlowPane flowPane = new FlowPane(Orientation.VERTICAL, 10, 10);
        flowPane.getChildren().addAll(textField, okButton);
        flowPane.setColumnHalignment(HPos.CENTER);
        flowPane.setAlignment(Pos.CENTER);
        Scene scene = new Scene(flowPane, 200, 200);
        Stage stage = new Stage();
        stage.setTitle("Select playlist name");
        stage.setScene(scene);
        stage.addEventHandler(CustomEventTypes.CLOSE_STAGE, event -> stage.close());
        return stage;
    }


}
