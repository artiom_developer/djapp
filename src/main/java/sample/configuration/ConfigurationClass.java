package sample.configuration;

import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseButton;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import sample.AudioPlayer;
import sample.utilities.CustomEventTypes;

@Configuration
@PropertySource("classpath:application.properties")
public class ConfigurationClass {

    @Bean
    public Label currentMusicLabel() {
        Label currentMusic = new Label();
        currentMusic.setMaxWidth(250);
        currentMusic.setAlignment(Pos.CENTER);
        return currentMusic;
    }

    @Bean
    public ListView<String> playlist(@Qualifier("genreComboBox") ComboBox<String> genreComboBox, AudioPlayer audioPlayer) {
        audioPlayer.getPlaylist().setMaxSize(250, 350);
        Event playButtonPressed = new Event(CustomEventTypes.SHOW_PAUSE_BUTTON);
        audioPlayer.getPlaylist().getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        audioPlayer.getPlaylist().setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1) {
                audioPlayer
                        .getPathForSelectedSong()
                        .ifPresent(selectedSong -> genreComboBox.getSelectionModel().select(selectedSong));
            }
            if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                audioPlayer.playMusic();
                audioPlayer.getPlaylist().fireEvent(playButtonPressed);
            }
        });
        audioPlayer.getPlaylist().setOnDragOver(audioPlayer.dragOverEventHandler());
        audioPlayer.getPlaylist().setOnDragDropped(audioPlayer.dragDropEventHandler());
        audioPlayer.getPlaylist().setOnKeyPressed(audioPlayer.deleteKeyEventHandler());
        return audioPlayer.getPlaylist();
    }

}
