package sample.configuration;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sample.AudioPlayer;

import java.io.File;
import java.util.List;

import static sample.utilities.Utilities.showAlert;

@Configuration
public class FilesManipulationPanelConfiguration {

    @Bean
    public Button importButton(@Value("${player.importButtonTitle}") String importButtonTitle, AudioPlayer audioPlayer) {
        Stage stage = new Stage();
        Button importButton = new Button(importButtonTitle);
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter =
                new FileChooser.ExtensionFilter("AudioFiles", "*.mp3", "*.mp4");
        fileChooser.getExtensionFilters().addAll(extensionFilter);
        ObservableList<String> items = audioPlayer.getPlaylist().getItems();
        importButton.setOnAction(event -> {
            List<File> musicFile = fileChooser.showOpenMultipleDialog(stage);
            if (musicFile != null) {
                for (File file : musicFile) {
                    if (!items.contains(file.getName())) {
                        audioPlayer.addSong(file);
                    } else showAlert("Song is already in playlist");
                }
            }
        });
        return importButton;
    }

    @Bean
    public Button savePlaylistButton(@Qualifier("createSavePlaylistStage") Stage stage) {
        Button saveButton = new Button("Save");
        saveButton.setOnMouseClicked(event -> {
            stage.show();
        });
        return saveButton;
    }

    @Bean
    public Button loadPlaylistButton(@Qualifier("createLoadPlaylistStage") Stage stage) {
        Button loadButton = new Button("Load");
        loadButton.setOnMouseClicked(event -> {
            stage.show();
        });
        return loadButton;
    }

    @Bean
    Button sortButton(AudioPlayer audioPlayer) {
        Button sortButton = new Button("Sort");
        sortButton.setOnMouseClicked(event -> {
        });
        return sortButton;
    }

    @Bean
    public FlowPane topWindowPane(@Qualifier("importButton") Button importButton,
                                  @Qualifier("savePlaylistButton") Button savePlaylistButton,
                                  @Qualifier("loadPlaylistButton") Button loadPlaylistButton,
                                  @Qualifier("sortButton") Button sortButton) {
        FlowPane saveImport = new FlowPane(Orientation.HORIZONTAL, 10, 10);
        saveImport.setAlignment(Pos.CENTER);
        saveImport.setColumnHalignment(HPos.CENTER);
        saveImport.setMaxWidth(250);
        saveImport.getChildren().addAll(importButton, savePlaylistButton, loadPlaylistButton, sortButton);
        return saveImport;
    }
}
