package sample;

import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Main extends Application {

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage){
        ApplicationContext ap = SpringApplication.run(Main.class);
        SpringStageLoader loader = ap.getBean(SpringStageLoader.class);
        Stage stage = new Stage();
        loader.loadMain(stage).show();
    }
}
