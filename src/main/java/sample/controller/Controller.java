package sample.controller;

import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Controller {

    private String windowTitle;
    private int windowWidth;
    private int windowHeight;

    @Autowired
    @Qualifier("topWindowPane")
    private FlowPane topWindowPane;

    @Autowired
    @Qualifier("playBackPane")
    private FlowPane playBackPane;

    @Autowired
    @Qualifier("currentMusicLabel")
    private Label currentMusicLabel;

    @Autowired
    @Qualifier("playlist")
    private ListView playlist;

    @Autowired
    @Qualifier("genrePane")
    private FlowPane genrePane;


    public Controller(@Value("${player.windowTitle}") String windowTitle,
                      @Value("${player.windowWidth}") String windowWidth,
                      @Value("${player.windowHeight}") String windowHeight) {
        this.windowTitle = windowTitle;
        this.windowWidth = Integer.parseInt(windowWidth);
        this.windowHeight = Integer.parseInt(windowHeight);
    }

    public Scene createPlayerWindow(Stage stage) {
        Scene scene = new Scene(createWindowPane(), windowWidth, windowHeight);
        stage.setTitle(windowTitle);
        return scene;
    }

    public FlowPane createWindowPane() {
        FlowPane windowPane = new FlowPane(Orientation.VERTICAL, 10, 10);
        windowPane.setAlignment(Pos.CENTER);
        windowPane.setColumnHalignment(HPos.CENTER);
        windowPane.getChildren().addAll(topWindowPane, playBackPane, currentMusicLabel, playlist, genrePane);
        return windowPane;
    }

}
