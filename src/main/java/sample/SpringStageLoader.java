package sample;

import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;
import sample.controller.Controller;

@Component
public class SpringStageLoader {

    Controller controller;

    public SpringStageLoader(Controller controller) {
        this.controller = controller;
    }

    public Stage loadMain(Stage stage) {
        stage.setScene(controller.createPlayerWindow(stage));
        stage.setOnHidden(event -> Platform.exit());
        return stage;
    }

}
