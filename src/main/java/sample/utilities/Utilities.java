package sample.utilities;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.*;
import java.util.Scanner;

public class Utilities {

    public static void showAlert(String alertText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.headerTextProperty().setValue(alertText);
        alert.show();
    }

    public static Button createButton(String imagePath) {
        File imageFile = new File(imagePath);
        Image image = new Image(imageFile.toURI().toString());
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(26);
        imageView.setFitWidth(26);
        Button button = new Button();
        button.setGraphic(imageView);
        return button;
    }

    public static Button createEnterGenreButton(String imagePath) {
        File imageFile = new File(imagePath);
        Image image = new Image(imageFile.toURI().toString());
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(15);
        imageView.setFitWidth(15);
        Button button = new Button();
        button.setGraphic(imageView);
        return button;
    }

    public static void createPlaylistFile(String playlistName) throws IOException {
        String playlistsFilePath = System.getenv("playlistsFilePath");
        File file = new File(playlistsFilePath);
        Writer writer = new BufferedWriter(new FileWriter(file, true));
        writer.append(playlistName);
        ((BufferedWriter) writer).newLine();
        writer.close();
    }

    public static void readFromPlaylistFile(File file, ComboBox<String> box) throws FileNotFoundException {
        Scanner sc = new Scanner(file);
        while (sc.hasNext()) {
            box.getItems().add(sc.nextLine());
        }

    }
}
