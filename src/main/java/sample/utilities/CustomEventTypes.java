package sample.utilities;

import javafx.event.Event;
import javafx.event.EventType;

public class CustomEventTypes {
    public static final EventType<Event> SHOW_PAUSE_BUTTON = new EventType<>("Show pause button");
    public static final EventType<Event> SHOW_PLAY_BUTTON = new EventType<>("Show play button");
    public static final EventType<Event> CLOSE_STAGE = new EventType<>("Close stage");
}
