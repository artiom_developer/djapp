package sample;

import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import sample.utilities.Utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Stream;

@Component
public class AudioPlayer {
    private String musicDataPath;

    @Getter
    private MediaPlayer mediaPlayer;
    @Getter
    private ListView<String> playlist = new ListView<>();
    @Getter
    private Map<String, String> fileMap = new HashMap<>();
    private Duration musicCurrentTime;
    private Optional<String> lastMusic;

    public AudioPlayer(@Value("musicDataPath") String musicDataPath) {
        this.musicDataPath = musicDataPath;
    }

    public void playMusic() {
        String selectedMusicFile = playlist.getSelectionModel().getSelectedItem();
        File file = new File(fileMap.get(selectedMusicFile));
        Media sound = new Media(file.toURI().toString());
        mediaPlayer = new MediaPlayer(sound);
        lastMusic.ifPresent(song -> {
            if (song.equals(selectedMusicFile)) {
                mediaPlayer.setStartTime(musicCurrentTime);
            }
        });
        mediaPlayer.play();
        lastMusic = Optional.of(selectedMusicFile);
    }

    public void stopCurrentTime() {
        musicCurrentTime = new Duration(0.00);
    }

    public void setMusicCurrentTime() {
        musicCurrentTime = mediaPlayer.getCurrentTime();
    }

    public boolean isItemSelected() {
        return playlist.getSelectionModel().getSelectedItem() != null;
    }

    public void addSong(File file) {
        playlist.getItems().add(file.getName());
        fileMap.putIfAbsent(file.getName(), file.getPath());
    }

    public EventHandler<DragEvent> dragOverEventHandler() {
        return event -> event.acceptTransferModes(TransferMode.MOVE);
    }

    public EventHandler<DragEvent> dragDropEventHandler() {
        return dragEvent -> {
            List<File> draggedFiles = dragEvent.getDragboard().getFiles();
            draggedFiles.forEach(file -> {
                final Stream<File> musicFiles;
                if (file.isDirectory()) {
                    musicFiles = Stream.of(file.listFiles());
                } else {
                    musicFiles = Stream.of(file);
                }
                musicFiles.forEach(this::addSong);
            });
            dragEvent.setDropCompleted(true);
        };
    }

    public EventHandler<KeyEvent> deleteKeyEventHandler() {
        return event -> {
            if (event.getCode() == KeyCode.DELETE) {
                playlist.getItems().removeAll(playlist.getSelectionModel().getSelectedItems());
            }
        };
    }

    public void filePutFromPlaylist(File file) {
        String fileName = file.getName().substring((file.getName().indexOf("-")) + 1);
        playlist.getItems().add(fileName);
        fileMap.putIfAbsent(fileName, file.getPath());
    }

    public Optional<String> getPathForSelectedSong() {
        return getSelectedSong().flatMap(selectedSong -> {
                    File file = new File(musicDataPath + selectedSong + ".txt");
                    if (file.exists()) {
                        try (Scanner sc = new Scanner(file)) {
                            return Optional.of(sc.nextLine());
                        } catch (FileNotFoundException e) {
                            return Optional.empty();
                        }
                    } else {
                        return Optional.empty();
                    }
                }
        );
    }

    public Optional<String> getSelectedSong() {
        return Optional.ofNullable(playlist.getSelectionModel().getSelectedItem());
    }

    public void createMusicFileTxt(String fileName, String genre, String filePath) {
        File outputFile = new File(musicDataPath + fileName + ".txt");
        try (PrintWriter musicFile = new PrintWriter(outputFile, "UTF-8")) {
            musicFile.println(filePath);
            musicFile.println(genre);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            Utilities.showAlert("Cannot create file in the target directory. Check if the target folder has permission for writing/creating files");
        }
    }
}
